# README #

AWS lamdba function to remove exif metadata from jpg files uploaded to s3.

Creates and configures the exitStripper lamdba function and source/destination s3 buckets. 

Two test IAM users (UserA and UserB) are also created, but require security credentials to be created before use. 

### Deployment ###

Assumes both Terraform and AWS client are installed and that the aws-cli is authenticated to your AWS account with appropirate IAM permissions.

Download/clone repo and deploy with standard terraform commands. 

Upload jpg file to source-bucket-a to trigger the exifStripper lambda, which saves the processed file to destination-bucket-b. 


### Versions ###

* Terraform v1.0.3
* aws-cli/2.2.25

