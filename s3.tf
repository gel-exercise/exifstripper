resource "aws_s3_bucket" "source_bucket" {
  bucket_prefix = "source-bucket-a-"
  acl           = "private"
  force_destroy = true
}

resource "aws_s3_bucket_public_access_block" "source_bucket" {
  bucket                  = aws_s3_bucket.source_bucket.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
  depends_on              = [aws_s3_bucket_notification.upload_notification]

}

resource "aws_s3_bucket_notification" "upload_notification" {
  bucket = aws_s3_bucket.source_bucket.id
  lambda_function {
    lambda_function_arn = aws_lambda_function.exif_stripper.arn
    events              = ["s3:ObjectCreated:*"]
    filter_suffix       = ".jpg"
  }
  depends_on = [aws_s3_bucket.source_bucket]
}

resource "aws_s3_bucket" "dest_bucket" {
  bucket_prefix = "destination-bucket-b-"
  acl           = "private"
  force_destroy = true
}

resource "aws_s3_bucket_public_access_block" "dest_bucket" {
  bucket                  = aws_s3_bucket.dest_bucket.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
  depends_on              = [aws_s3_bucket.dest_bucket]
}