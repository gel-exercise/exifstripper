output "Source-Bucket" {
  value = aws_s3_bucket.source_bucket.id
}

output "Destination-Bucket" {
  value = aws_s3_bucket.dest_bucket.id
}
