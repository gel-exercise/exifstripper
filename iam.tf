resource "aws_iam_role" "exif_stripper_role" {
  name               = "exifStripperRole"
  assume_role_policy = data.aws_iam_policy_document.exif_stipper_assume_role_policy.json
  inline_policy {
    name   = "exifStripperPolicy"
    policy = data.aws_iam_policy_document.exif_stripper_inline_policy.json
  }
}

data "aws_iam_policy_document" "exif_stipper_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "exif_stripper_inline_policy" {
  statement {
    effect = "Allow"
    actions = [
      "logs:PutLogEvents",
      "logs:CreateLogGroup",
      "logs:CreateLogStream"
    ]
    resources = ["arn:aws:logs:*:*:*"]
  }
  statement {
    effect    = "Allow"
    actions   = ["s3:ListBucket"]
    resources = [aws_s3_bucket.source_bucket.arn, aws_s3_bucket.dest_bucket.arn]
  }
  statement {
    effect    = "Allow"
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.source_bucket.arn}/*"]
  }
  statement {
    effect    = "Allow"
    actions   = ["s3:PutObject"]
    resources = ["${aws_s3_bucket.dest_bucket.arn}/*"]
  }
}