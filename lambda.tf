locals {
  zipfile_path = "${path.module}/exifStripper.zip"
}

resource "aws_lambda_function" "exif_stripper" {
  function_name = "exifStripper"
  role          = aws_iam_role.exif_stripper_role.arn
  handler       = "lambda_function.lambda_handler"
  filename      = local.zipfile_path
  runtime       = "python3.8"
  timeout       = 10
  environment {
    variables = {
      "DEST_BUCKET" = aws_s3_bucket.dest_bucket.bucket
    }
  }
}

resource "aws_lambda_permission" "allow_s3_events" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.exif_stripper.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.source_bucket.arn
}
