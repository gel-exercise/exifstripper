# UserA
resource "aws_iam_user" "user_a" {
  name = "UserA"
}

# resource "aws_iam_access_key" "user_a_key" {
#   user = aws_iam_user.user_a.name
# }

resource "aws_iam_user_policy" "user_a_policy" {
  name   = "s3bucketAReadWrite"
  user   = aws_iam_user.user_a.name
  policy = data.aws_iam_policy_document.user_a_policy_document.json
}

data "aws_iam_policy_document" "user_a_policy_document" {
  statement {
    effect    = "Allow"
    actions   = ["s3:ListAllMyBuckets"]
    resources = ["*"]
  }
  statement {
    effect    = "Allow"
    actions   = ["s3:ListBucket"]
    resources = [aws_s3_bucket.source_bucket.arn]
  }
  statement {
    effect = "Allow"
    actions = [
      "s3:GetObject",
      "s3:PutObject"
    ]
    resources = ["${aws_s3_bucket.source_bucket.arn}/*"]
  }
}


# UserB
resource "aws_iam_user" "user_b" {
  name = "UserB"
}

# resource "aws_iam_access_key" "user_b_key" {
#   user = aws_iam_user.user_b.name
# }

resource "aws_iam_user_policy" "user_b_policy" {
  name   = "s3bucketBRead"
  user   = aws_iam_user.user_b.name
  policy = data.aws_iam_policy_document.user_b_policy_document.json
}

data "aws_iam_policy_document" "user_b_policy_document" {
  statement {
    effect    = "Allow"
    actions   = ["s3:ListAllMyBuckets"]
    resources = ["*"]
  }
  statement {
    effect    = "Allow"
    actions   = ["s3:ListBucket"]
    resources = [aws_s3_bucket.dest_bucket.arn]
  }
  statement {
    effect = "Allow"
    actions = [
      "s3:GetObject",
    ]
    resources = ["${aws_s3_bucket.dest_bucket.arn}/*"]
  }
}